from flask import Flask
import numpy as np
app = Flask(__name__)

@app.route("/")
def hello():
    matrix = np.arange(0, 9).reshape(3,3)
    print("matrix:\n", matrix)
    # print("element at [2, 3]:\n", matrix[2,3])      # Index out of bounds
    print("element at [0, 0]:\n", matrix[0,0])
    return app.send_static_file("index.html")